﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

namespace Calculations
{
    class DBConnection
    {
        private static readonly DBConnection _instance = new DBConnection();
        private static NpgsqlConnection conn;
        private const string HOST = "localhost";
        private const string DATABASE = "db_projet_rao";
        private const string USERNAME = "postgres";
        private const string PASSWORD = "root";

        private DBConnection()
        {
            var connString = string.Format("Host={0};Username={1};Password={2};Database={3}", HOST, USERNAME, PASSWORD, DATABASE);
            conn = new NpgsqlConnection(connString);
            conn.Open();
            if (conn.State == System.Data.ConnectionState.Open)
                Console.WriteLine("Connection to database successful !");

                /*
                // Insert some data
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "INSERT INTO data (some_field) VALUES (@p)";
                    cmd.Parameters.AddWithValue("p", "Hello world");
                    cmd.ExecuteNonQuery();
                }

                // Retrieve all rows
                using (var cmd = new NpgsqlCommand("SELECT * FROM computed", conn))
                using (var reader = cmd.ExecuteReader())
                    while (reader.Read())
                        Console.WriteLine(reader.GetString(0));*/
        }

        public static DBConnection Instance
        {
            get { return _instance; }
        }

        public List<MetricType> GetMetricTypes()
        {
            NpgsqlCommandBuilder test = new NpgsqlCommandBuilder();
            Console.WriteLine(test.GetInsertCommand());

            return null;
        }

        public bool Insert(List<ComplexCalculation> calcs)
        {
            using (var cmd = new NpgsqlCommand())
            {
                Console.WriteLine("Retrieving DB connection.");
                cmd.Connection = conn;
                Console.WriteLine("DB connection retrieved.");
                cmd.CommandText =
                    "INSERT INTO computed (computed_id_computed, value_computed, computed_type, created_at_computed, type_id_types)" +
                    "VALUES (@computed_id_computed, @value_computed, @computed_type, @created_at_computed, @type_id_types)";
                Console.WriteLine("Start insertions.");
                
                calcs.ForEach(c =>
                {
                    cmd.Parameters.AddWithValue("computed_id_computed", c.Id);
                    cmd.Parameters.AddWithValue("value_computed", c.Value);
                    cmd.Parameters.AddWithValue("computed_type", c.ComputedType);
                    cmd.Parameters.AddWithValue("created_at_computed", c.CreatedAt);
                    cmd.Parameters.AddWithValue("type_id_types", c.TypeId);
                    Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    cmd.ExecuteNonQuery();
                    Console.WriteLine("\tInsertion Successful.");
                });
                Console.WriteLine("Insertions done.");
            }
            return true;
        }
    }
}