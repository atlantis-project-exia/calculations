﻿using System;
using System.Runtime.Serialization;

namespace Calculations
{
    public class ComplexCalculation
    {
        public Guid Id { get; set; }
        public byte[] Value { get; set; }
        public DateTime CreatedAt { get; set; }
        public Guid TypeId { get; set; }
        public string ComputedType { get; set; }
        //[DataMember] public uint DeviceId { get; set; }
    }
}
