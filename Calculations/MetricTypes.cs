﻿using System.Collections.Generic;
using System;

namespace Calculations
{
    public partial class MetricTypes
    {
        private static readonly MetricTypes _instance = new MetricTypes();
        private List<MetricType> metricTypes = new List<MetricType>();

        private MetricTypes() { }

        public static MetricTypes Instance
        {
            get { return _instance; }
        }

        public List<MetricType> MetricTypeList
        {
            get { return metricTypes; }
            set { metricTypes = value; }
        }
    }
}
