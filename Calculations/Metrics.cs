﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculations
{
    class Metrics
    {
        private static readonly Metrics _instance = new Metrics();
        private Metrics() { }
        private List<Metric> metric = new List<Metric>();

        public static Metrics Instance
        {
            get { return _instance; }
        }

        public List<Metric> MetricList
        {
            get { return metric; }
            set { metric = value; }
        }
    }
}
