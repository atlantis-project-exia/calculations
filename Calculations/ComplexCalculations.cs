﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Calculations
{
    class ComplexCalculations
    {
        private static readonly ComplexCalculations _instance = new ComplexCalculations();
        private List<ComplexCalculation> calculations = new List<ComplexCalculation>();
        private ArrayList finalMetrics;
        private ComplexCalculations() {
            finalMetrics = new ArrayList();
        }

        public static ComplexCalculations Instance
        {
            get { return _instance; }
        }

        public List<ComplexCalculation> MetricList
        {
            get { return calculations; }
        }

        private T ConvertByte<T>(byte[] toConvert)
        {
            return (T)Convert.ChangeType(toConvert, typeof(T));
        }

        private void ConvertMetrics(List<byte[]> metrics, string _type)
        {
            Type type;
            if ((type = Type.GetType(_type)) == null)
            {
                Console.WriteLine("Problem with type {0}. Exiting now.", _type);
                Environment.Exit(-1);
            }
            else
                Console.WriteLine("Converting...");

            if (_type == "System.Int32" && _type != "System.Double")
            {
                metrics.ForEach(m =>
                {
                    finalMetrics.Add(BitConverter.ToInt32(m));
                });
            } else if (_type == "System.Single")
            {
                metrics.ForEach(m =>
                {
                    finalMetrics.Add(BitConverter.ToSingle(m));
                });
            } else if (_type == "System.String")
            {
                metrics.ForEach(m =>
                {
                    finalMetrics.Add(BitConverter.ToString(m));
                });
            }
            Console.WriteLine("Converting Done");
        }

        public void Average(List<byte[]> metrics, string _type, Guid typeId)
        {
            if (_type != "System.Single" && _type != "System.Int32" && _type != "System.Double")
            {
                Console.WriteLine("Nothing to do, there are no numbers.");
                return;
            }
            if (metrics.Count == 0)
            {
                Console.WriteLine("Nothing to do, there are no metrics.");
                return;
            }
            ConvertMetrics(metrics, _type);
            Type type;
            if ((type = Type.GetType(_type)) == null)
            {
                Console.WriteLine("Problem with type {0}. Exiting now.", _type);
                Environment.Exit(-1);
            }
            else
                Console.WriteLine("Averaging...");

            dynamic m;
            dynamic sum = Convert.ChangeType(0, type);
            for (int i = 0; i < finalMetrics.Count; i++) {
                m = Convert.ChangeType(finalMetrics[i], type);
                sum += m;
            }

            byte[] value = BitConverter.GetBytes(sum / metrics.Count);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(value);
           
            calculations.Add(new ComplexCalculation { Id = Guid.NewGuid(), Value = value, TypeId = typeId, CreatedAt = DateTime.Now, ComputedType = "Average"});
            Console.WriteLine("Averaging Done");
        }

        public void Sum(List<byte[]> metrics, string _type)
        {
            if (_type != "System.Single" && _type != "System.Int32" && _type != "System.Double")
            {
                Console.WriteLine("Nothing to do, there are no numbers.");
                return;
            }
            if (metrics.Count == 0)
            {
                Console.WriteLine("Nothing to do, there are no metrics.");
                return;
            }
            /*ConvertMetrics(metrics, _type);
            int sum = 0;
            metrics.ForEach(m => sum += m);
            calculations.Add(new ComplexCalculation { Id = Guid.NewGuid(), Value = sum.ToString(), TypeId = typeId, CreatedAt = DateTime.Now, ComputedType = "Average" });*/
        }

        public void Median(List<byte[]> metrics, string _type, Guid typeId)
        {
            if (_type != "System.Single" && _type != "System.Int32" && _type != "System.Double")
            {
                Console.WriteLine("Nothing to do, there are no numbers.");
                return;
            }
            if (metrics.Count == 0)
            {
                Console.WriteLine("Nothing to do, there are no metrics.");
                return;
            }
            /*ConvertMetrics(metrics, _type);
            float med;
            metrics.Sort();
            med = metrics.Count % 2 == 1 ?
                metrics[metrics.Count / 2] :
                ((metrics[metrics.Count / 2] + metrics[metrics.Count / 2 + 1]) / 2);
            calculations.Add(new ComplexCalculation { Id = Guid.NewGuid(), Value = med.ToString(), TypeId = typeId, CreatedAt = DateTime.Now, ComputedType = "Average"});*/
        }
    }
}
