﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Calculations
{
    class Metric
    {
        public byte[] Value { get; set; }
        public Guid Id { get; set; }
        public Guid MetricTypeId { get; set; }
        public Guid DeviceId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
