﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Linq;

namespace Calculations
{
    class Program
    {
        public static DBConnection dbInstance;
        public static ComplexCalculations complexCalculations;
        public static MetricTypes metricTypes;
        public static MetricTypeContext metricTypeContext;
        public static Metrics metrics;

        static void Main(string[] args)
        {
            dbInstance = DBConnection.Instance;
            complexCalculations = ComplexCalculations.Instance;
            metricTypes = MetricTypes.Instance;
            metrics = Metrics.Instance;

            dbInstance.GetMetricTypes();

            MetricComparison mc = new MetricComparison();

            Random rnd = new Random();

            metrics.MetricList.Add(new Metric() { MetricTypeId = new Guid("10000000-0000-0000-0000-000000000000"), Id = Guid.NewGuid(), DeviceId = Guid.NewGuid(), CreatedAt = DateTime.Now, Value = BitConverter.GetBytes(rnd.Next(0, 30)) });
            metrics.MetricList.Add(new Metric() { MetricTypeId = new Guid("10000000-0000-0000-0000-000000000000"), Id = Guid.NewGuid(), DeviceId = Guid.NewGuid(), CreatedAt = DateTime.Now, Value = BitConverter.GetBytes(rnd.Next(0, 30)) });
            metrics.MetricList.Add(new Metric() { MetricTypeId = new Guid("10000000-0000-0000-0000-000000000000"), Id = Guid.NewGuid(), DeviceId = Guid.NewGuid(), CreatedAt = DateTime.Now, Value = BitConverter.GetBytes(rnd.Next(0, 30)) });
            metrics.MetricList.Add(new Metric() { MetricTypeId = new Guid("10000000-0000-0000-0000-000000000000"), Id = Guid.NewGuid(), DeviceId = Guid.NewGuid(), CreatedAt = DateTime.Now, Value = BitConverter.GetBytes(rnd.Next(0, 30)) });
            metrics.MetricList.Add(new Metric() { MetricTypeId = new Guid("20000000-0000-0000-0000-000000000000"), Id = Guid.NewGuid(), DeviceId = Guid.NewGuid(), CreatedAt = DateTime.Now, Value = BitConverter.GetBytes(rnd.Next(0, 30)) });

            metrics.MetricList.Sort(mc);

            var toSend = new List<Metric>();
            var metricsToSend = new List<byte[]>();

            foreach (MetricType mt in metricTypes.MetricTypeList)
            {
                toSend.Clear();
                toSend = metrics.MetricList.FindAll(m => m.MetricTypeId == mt.Id);
                
                toSend.ForEach(t => metricsToSend.Add(t.Value));
                complexCalculations.Average(metricsToSend, mt.Type, mt.Id);
            }
            //            complexCalculations.Average(metrics, metricTypes.MetricTypeList[(int)typeId].Type, typeId);
            /*complexCalculations.Sum(metrics, metricTypes.MetricList[typeId].Type);
            complexCalculations.Median(metrics, metricTypes.MetricList[typeId].Type);*/
            complexCalculations.MetricList.ForEach(metric => Console.WriteLine(BitConverter.ToInt32(metric.Value).ToString()));
            dbInstance.Insert(complexCalculations.MetricList);
        }
    }

    class MetricComparison : IComparer<Metric>
    {
        public int Compare(Metric m1, Metric m2)
        {
            BigInteger bId1 = new BigInteger(m1.MetricTypeId.ToByteArray());
            BigInteger bId2 = new BigInteger(m2.MetricTypeId.ToByteArray());
            if (bId1.CompareTo(bId2) == 0)
                return BitConverter.ToInt32(m1.Value) - BitConverter.ToInt32(m2.Value);
            return bId1.CompareTo(bId2);
        }
    }
}
