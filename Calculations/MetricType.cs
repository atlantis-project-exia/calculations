﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Runtime.Serialization;

namespace Calculations
{
    public class MetricType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public string Type { get; set; }
    }

    public class MetricTypeContext : DbContext
    {
        public DbSet<MetricType> MetricTypes;
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=db_projet_rao;Username=postgres;Password=root");
    }
}
